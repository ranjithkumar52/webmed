<!DOCTYPE html>
<html>
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Murach's Java Servlets and JSP</title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <!--custom css-->
        <link rel="stylesheet" href="css/styles.css">
    </head>
    <body>
        <!-- nav bar logo Insurance eligibility, billing, e-prescriptions and the login at the end -->        
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">WebMED</a>
                </div>
                <ul class="nav navbar-nav">
                    <li class="active"><a href="index.jsp">Home</a></li>
                    <li><a href="Insurance.jsp">Insurance</a></li>
                    <li><a href="Billing.jsp">Billing</a></li>
                    <li><a href="prescriptions.jsp">Prescriptions</a></li>
                    <li><a href="about.jsp">About</a></li>
                    <li><a href="Contact.jsp">Contact</a></li>
                    <li><a href="Login.jsp">Login</a></li>
                    <li><a href="signup.jsp">Signup</a></li>
                    <li><a href="admin/login.jsp">Admin</a></li>
                </ul>
            </div>
        </nav>
        <!-- JUMBOTRON -->
        <div class="jumbotron text-center">
            <h1>Welcome to WebMED</h1>
            <p>WebMED is an Electronic Health Records application for small doctors office and clinics.</p> 
            <p>It allows medical professionals to share medical records internally and externally</p>
        </div>
        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>		
        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </body>
</html>