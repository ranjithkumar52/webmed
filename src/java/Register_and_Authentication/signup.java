package Register_and_Authentication;

import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.http.*;

public class signup extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession session = request.getSession();

        String url = "/signup.jsp";

        // get current action
        //String action = request.getParameter("action");
        
        String message;
        Boolean emailExists;
        int success = 0;
        String firstName = request.getParameter("firstName");
        String lastName = request.getParameter("lastName");
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        User user = new User(firstName, lastName, email, password);
        emailExists = UserDB.emailExists(email);
        if(!emailExists){
            success = UserDB.insert(user);
            if(success != 0){
                message = "Sign up successful";
                session.setAttribute("message", message);
            }
            else{
                message = "Sign up failed to record in the database. Check if database is live";
                session.setAttribute("message", message);
            }
        }
        else{
            message = "Email already registered. Please go back to login page";
            session.setAttribute("message", message);
        }
        
        

        /*
        if (action == null) {
            action = "display_users";  // default action
        }
        
        // perform action and set URL to appropriate page
        if (action.equals("display_users")) {            
            // get list of users
            ArrayList<User> users = UserDB.selectUsers();            

            // set as a request attribute
            // forward to index.jsp
        } 
        else if (action.equals("display_user")) {
            // get user for specified email
            // set as session attribute
            // forward to user.jsp
        }
        else if (action.equals("update_user")) {
            // update user in database
            // get current user list and set as request attribute
            // forward to index.jsp
        }
        else if (action.equals("delete_user")) {
            // get the user for the specified email
            // delete the user            
            // get current list of users
            // set as request attribute
            // forward to index.jsp
        }*/
        getServletContext()
                .getRequestDispatcher(url)
                .forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response)
            throws ServletException, IOException {
        doPost(request, response);
    }

}
