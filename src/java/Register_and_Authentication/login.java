package Register_and_Authentication;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class login extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession session = request.getSession();
        boolean emailExists;
        User user = new User();
        String message;
        String url = "/Login.jsp";
        // get current action
        //String action = request.getParameter("action");
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        //validate email and password
        emailExists = UserDB.emailExists(email);
        //success case
        if(emailExists){
            user = UserDB.selectUser(email);
            String password_record = user.getPassword();
            if(password_record.equals(password)){
                message = "login Successful";
                session.setAttribute("message", message);
            }
            else{
                message = "Incorrect Password";
                session.setAttribute("message", message);
            }
            
        }
        //fail case
        else{
            message = "login Failed";
            session.setAttribute("message", message);
        }        
        getServletContext()
                .getRequestDispatcher(url)
                .forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response)
            throws ServletException, IOException {
        doPost(request, response);
    }

}
