<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Murach's Java Servlets and JSP</title>
        <!-- bootstrap files -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <!--custom css-->
        <link rel="stylesheet" href="css/styles.css">
    </head>
    <body>
        <!-- nav bar logo Insurance eligibility, billing, e-prescriptions and the login at the end -->
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">WebMED</a>
                </div>
                <ul class="nav navbar-nav">
                    <li><a href="index.jsp">Home</a></li>
                    <li><a href="Insurance.jsp">Insurance</a></li>
                    <li><a href="Billing.jsp">Billing</a></li>
                    <li><a href="prescriptions.jsp">Prescriptions</a></li>
                    <li><a href="about.jsp">About</a></li>
                    <li class="active"><a href="Contact.jsp">Contact</a></li>
                    <li><a href="Login.jsp">Login</a></li>
                    <li><a href="signup.jsp">Signup</a></li>
                    <li><a href="admin/login.jsp">Admin</a></li>
                </ul>
            </div>
        </nav>

        <!-- JUMBOTRON -->
        <div class="jumbotron text-center">
            <h1>Welcome to WebMED</h1>
            <p>13113 Atkins Circle Drive Charlotte, NC 28277</p> 
            <p> 1-800-222-HELP </p>
        </div>
        <div class="container">
            <form action="EmailListServlet" method="post">
                <div class="form-group">
                    <label>Email address</label>
                    <input type="email" name="email" class="form-control" placeholder="name@example.com">
                </div>            
                <div class="form-group">
                    <label>First Name</label>
                    <input type="text" name="firstName" class="form-control" placeholder="First Name">
                </div>
                <div class="form-group">
                    <label>Last Name</label>
                    <input type="text" name="lastName" class="form-control" placeholder="Last Name">
                </div>
                <div class="col-sm-offset-4 col-sm-4">
                    <button type="submit" class="btn btn-primary btn-block">Send Email</button>
                    <p class="text-center">${message}</p>                    
                </div>
            </form>
        </div>
        <!-- always place .js files here -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" ></script>

    </body>
</html>