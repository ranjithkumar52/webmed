<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Murach's Java Servlets and JSP</title>
        <!-- bootstrap files -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <!-- custom css -->
        <link rel="stylesheet" href="css/styles.css">
    </head>
    <body>
        <!-- nav bar logo Insurance eligibility, billing, e-prescriptions and the login at the end -->
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">WebMED</a>
                </div>
                <ul class="nav navbar-nav">
                    <li><a href="index.jsp">Home</a></li>
                    <li class="active"><a href="Insurance.jsp">Insurance</a></li>
                    <li><a href="Billing.jsp">Billing</a></li>
                    <li><a href="prescriptions.jsp">Prescriptions</a></li>
                    <li><a href="about.jsp">About</a></li>
                    <li><a href="Contact.jsp">Contact</a></li>
                    <li><a href="Login.jsp">Login</a></li>
                    <li><a href="signup.jsp">Signup</a></li>
                    <li><a href="admin/login.jsp">Admin</a></li>
                </ul>
            </div>
        </nav>

        <!-- JUMBOTRON -->
        <div class="jumbotron text-center">
            <h1>Welcome to WebMED</h1>
            <p>Allows doctors offices, clinics and pharmacist to validate if a patient has insurance.</p> 
        </div>


        <!-- always place .js files here -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>

    </body>
</html>