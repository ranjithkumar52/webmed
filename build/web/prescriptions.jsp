<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Murach's Java Servlets and JSP</title>
        <!-- bootstrap files -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <!-- custom css -->
        <link rel="stylesheet" href="css/styles.css">
    </head>
    <body>
        <!-- nav bar logo Insurance eligibility, billing, e-prescriptions and the login at the end -->        
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">WebMED</a>
                </div>
                <ul class="nav navbar-nav">
                    <li><a href="index.jsp">Home</a></li>
                    <li><a href="Insurance.jsp">Insurance</a></li>
                    <li><a href="Billing.jsp">Billing</a></li>
                    <li class="active"><a href="prescriptions.jsp">Prescriptions</a></li>
                    <li><a href="about.jsp">About</a></li>
                    <li><a href="Contact.jsp">Contact</a></li>
                    <li><a href="Login.jsp">Login</a></li>
                    <li><a href="signup.jsp">Signup</a></li>
                    <li><a href="admin/login.jsp">Admin</a></li>
                </ul>
            </div>
        </nav>

        <!-- JUMBOTRON -->
        <div class="jumbotron text-center">
            <h1>Welcome to WebMED</h1>
            <p>See your prescription and perscribing doctor</p> 
        </div>

        <div class="container">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th scope="col">drugid#</th>
                        <th scope="col">Drug Name</th>
                        <th scope="col">Expiry Date</th>
                        <th scope="col">Price($)</th>

                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th scope="row">1</th>
                        <td>Drug Name #1</td>
                        <td>2 years</td>
                        <td>100</td>

                    </tr>
                    <tr>
                        <th scope="row">2</th>
                        <td>Drug Name #2</td>
                        <td>3 years</td>
                        <td>200</td>

                    </tr>
                    <tr>
                        <th scope="row">3</th>
                        <td>Drug Name #3</td>
                        <td>4 years</td>
                        <td>300</td>

                    </tr>
                </tbody>
            </table>
        </div>
        <div class="container">
            <h4 class="text-center">Please enter the details below to buy your prescriptions</h4>
            <form action="" method="post">
                <div class="form-group">
                    <div class="col-sm-6">			   				   
                        <input type="text" name="drugid" class="form-control"   placeholder="Enter Drug Id">
                    </div>			    
                </div><!-- end form-group -->
                <div class="form-group">
                    <div class="col-sm-6">			   				   
                        <input type="text" name="drugname" class="form-control" placeholder="Enter Drug Name">
                    </div>			    
                </div><!-- end form-group -->
                <br>
                <div class="form-group">
                    <div class="col-sm-6">			   				   
                        <input type="email" name="email" class="form-control" placeholder="Enter User Email">
                    </div>			    
                </div><!-- end form-group -->
                <div class="form-group">
                    <div class="col-sm-6">			   				   
                        <input type="password" name="password" class="form-control" placeholder="Enter User password">
                    </div>			    
                </div><!-- end form-group -->
                <br><br>
                <div class="col-sm-offset-4 col-sm-4">
                    <button type="submit" class="btn btn-primary btn-block">Buy</button>
                </div>
                
            </form>
        </div><!-- end container -->


        <!-- always place .js files here -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>

    </body>
</html>